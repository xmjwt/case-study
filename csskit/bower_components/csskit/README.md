======
csskit
======

SASS Based, BEM, OOCSS Framework

sass command line:
sass input.scss:output.css

with loadpath:
sass -I path/to/resource input.scss:output.css

watching for changes:
sass -I path/to/resource --watch input.scss:output.css
